SemVer File Server
==================
<span id="pipeline-badge">![pipeline badge](https://gitlab.com/nTopus/semver-file-server/badges/main/pipeline.svg)</span>
<span id="site">
  &mdash; [Web Page](https://nTopus.gitlab.io/semver-file-server)
</span>
<span id="repository">
  &mdash; [Fork me](https://gitlab.com/nTopus/semver-file-server)
</span>

<center id="logo">
<img src="./page-assets/semver-file-server.svg" width="500" alt="SemVer File Server's Logo">
</center>

This project provides a docker image to help the publishing of versioned static files like js libs, shared CSS, micro-frontend components, and more.
You can use it for a public service or behind a protected service, with a configurable CORS header.

So... Try it:
```sh
$ docker run \
  -p 8080:80 \
  -v /path/to/public-dir:/usr/share/nginx/html \
  -e CORS_RE='^https?:\/\/([a-z]+\.)*example\.com(:\d+)?$' \
  aurium/semver-file-server
```
...then visit `http://localhost:8080/inner/path/to/versioned-thing`

How to Publish
--------------

Your `public-dir` should have versioned files with `@<MAJOR>.<MINOR>.<PATH>` just before the extension and versioned directories with `@<MAJOR>.<MINOR>.<PATH>` on the end of the name.
See [this example directory](https://gitlab.com/nTopus/semver-file-server/-/tree/main/test/fixtures/sample-fs).

How It Works
------------

Like most public JS CDN, when you ask for some file, it redirects you to the path of the right file in the last version.

It expects you to publish files or directories appended with `@<VERSION>`.

So you can request without version definition, defining only the major with `@<MAJOR>` or `@<MAJOR>.*.*`., or de major and minor as well. Then you will get redirected to the correct path.

Features:
  * ☑️ Redirects requests without version defined to the right path.
  * ☑️ Redirects requests with partial version defined to the right path.
  * ☑️ Redirects requests without file to the default filename `index.js`.
  * ☑️ Redirects requests without file to the `package.json`'s defined main file.
  * 🚧 Suport other [semver range notations](https://docs.npmjs.com/cli/v6/using-npm/semver), besides `*`.
  * 🚧 Cache response.
  * ☑️ Support file browse, prefixing the URL path with `/browse/`.

It is done by NGINX with some "njs scripting language". It has nodejs unit tests and curl based behavior tests.


--------------------------------------------------------------------------------


Build Image
-----------

Just run `./build`


Publishing
----------

When deploying an image of this service, you shoud:
  * Set a volume with you versioned files to `/usr/share/nginx/html`
  * Set `CORS_RE` env var. A good value is: `^https?:\/\/((127\.0\.\d+\.\d+|(\w+\.)*localhost)(:\d+)?)$`


Hacking
-------

Edit what you want, update the `VERSION.txt`, commit, push and open a [MR](https://gitlab.com/nTopus/semver-file-server/-/merge_requests).


Testing
-------

Just run `./test/test.sh`
If you want to repeat the tests without docker rebuild, run: `./test/test.sh --no-rebuild`

Run `./test/test.sh --help` for more usage help.
