FROM nginx:stable-alpine

COPY nginx-conf/1-ensure-vars.sh  /docker-entrypoint.d/1-ensure-vars.sh
COPY nginx-conf/10-enable-njs.sh  /docker-entrypoint.d/10-enable-njs.sh
COPY nginx-conf/default.template  /etc/nginx/templates/default.conf.template
COPY nginx-conf/browser.js        /etc/nginx/conf.d/_browser.js
COPY nginx-conf/semver.js         /etc/nginx/conf.d/semver.js
COPY nginx-conf/browser-tmpl.html /browser-tmpl.html
COPY VERSION.txt                  /VERSION.txt
COPY page-assets/semver-file-server.svg /logo.svg
COPY page-assets/style.css              /style.css

RUN sed -ri "s/(%SRV_VERSION%)/$(cat /VERSION.txt)/" /etc/nginx/conf.d/semver.js

RUN grep -v 'export default' /etc/nginx/conf.d/semver.js \
    > /etc/nginx/conf.d/browser.js
RUN echo "const VERSION = '$(cat /VERSION.txt)'" \
    >> /etc/nginx/conf.d/browser.js
RUN echo "const LOGO = '$(base64 -w0 /logo.svg)'" \
    >> /etc/nginx/conf.d/browser.js
RUN echo -n "const CSS = '" \
    >> /etc/nginx/conf.d/browser.js
RUN echo $(cat /style.css) | \
    sed -r "s/\s*\{\s*/{/g; s/;?\s*}\s*/}/g; s/(:|;)\s*/\1/g; s/$/'/" \
    >> /etc/nginx/conf.d/browser.js
RUN echo "function buildPage(title, body){return \`$(cat browser-tmpl.html)\`}" \
    >> /etc/nginx/conf.d/browser.js
RUN cat /etc/nginx/conf.d/_browser.js \
    >> /etc/nginx/conf.d/browser.js

RUN rm /etc/nginx/conf.d/_browser.js /style.css

# Allows to remove NGINX version header, but the binary is not compatible right now:
# RUN apk update && apk add nginx-mod-http-headers-more

ENV SRV_VERSION=0.1.1
