function test_request_root_path() {
    req "http://localhost:$PORT/"
    match $LAST_RES_HEADER 'HTTP/1.1 302'
    match $LAST_RES_HEADER 'Location: /browse'
}

function test_real_path_without_leaf_and_version_must_not_crash() {
    req "http://localhost:$PORT/libA"
    match $LAST_RES_HEADER 'HTTP/1.1 404'
}

function test_nonexistent_path_must_not_crash() {
    req "http://localhost:$PORT/nothing"
    match $LAST_RES_HEADER 'HTTP/1.1 404'
}

function test_redirect_without_wildcard() {
    req "http://localhost:$PORT/libB"
    match $LAST_RES_HEADER 'HTTP/1.1 302'
    match $LAST_RES_HEADER 'Location: /libB@1.1.2/index.js'
    req "http://localhost:$PORT/someDir/libD"
    match $LAST_RES_HEADER 'HTTP/1.1 302'
    match $LAST_RES_HEADER 'Location: /someDir/libD@1.0.0/index.js'
}

function test_redirect_with_wildcard() {
    req "http://localhost:$PORT/libB@1.*"
    match $LAST_RES_HEADER 'HTTP/1.1 302'
    match $LAST_RES_HEADER 'Location: /libB@1.1.2/index.js'
}

function test_redirect_with_old_minor() {
    req "http://localhost:$PORT/libB@1.1.*"
    match $LAST_RES_HEADER 'HTTP/1.1 302'
    match $LAST_RES_HEADER 'Location: /libB@1.1.2/index.js'
}

function test_redirect_with_old_minor_without_wildcard() {
    req "http://localhost:$PORT/libB@1.1"
    match $LAST_RES_HEADER 'HTTP/1.1 302'
    match $LAST_RES_HEADER 'Location: /libB@1.1.2/index.js'
}

function test_redirect_without_filename_with_bar() {
    req "http://localhost:$PORT/libB/"
    match $LAST_RES_HEADER 'Location: /libB@1.1.2/index.js'
}

function test_redirect_without_filename_and_bar() {
    req "http://localhost:$PORT/libB"
    match $LAST_RES_HEADER 'Location: /libB@1.1.2/index.js'
}

function test_redirect_without_filename_and_bar_with_minor() {
    req "http://localhost:$PORT/libB@1.0"
    match $LAST_RES_HEADER 'Location: /libB@1.0.1/index.js'
}

function test_redirect_libC_without_filename_having_pkg_json() {
    req "http://localhost:$PORT/libC"
    match $FIXTURES/sample-fs/libC@3.0.0/package.json '"browser": "dist/lib-front.js"'
    match $LAST_RES_HEADER 'Location: /libC@3.0.0/dist/lib-front.js'
}

function test_redirect_libCv2_without_filename_having_pkg_json() {
    req "http://localhost:$PORT/libC@2"
    match $FIXTURES/sample-fs/libC@2.0.0/package.json '"main": "./lib.js"'
    match $LAST_RES_HEADER 'Location: /libC@2.0.0/lib.js'
}

function test_redirect_libCv1_without_filename_having_pkg_json() {
    req "http://localhost:$PORT/libC@1"
    match $FIXTURES/sample-fs/libC@1.0.0/package.json '"main": "/main.js"'
    match $LAST_RES_HEADER 'Location: /libC@1.0.0/main.js'
}
