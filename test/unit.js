const { readFileSync } = require('fs')
const assert = require('assert')

// Loads the server code:
const code = readFileSync(__dirname+'/../nginx-conf/semver.js', 'utf8')
.replace(/\/usr\/share\/nginx\/html/, __dirname+'/fixtures/sample-fs')
.replace(/\nexport default/s, 'return')
const server = new Function('require', code)
const { findVersionedStep, findVersion, findVersionFile, response } = server(require)

// Request moker:
const lastReturn = {}
function fakeReq(uri, path) {
    return {
        uri, path,
        headersOut: {},
        return(code, data) {
            lastReturn.code = code
            lastReturn.data = data
        }
    }
}

function findVersionReturn(path) {
  findVersionFile(fakeReq(null, path))
  return lastReturn
}

function responseReturn(uri) {
  response(fakeReq(uri))
  return lastReturn
}

let contextGroup = ''
function equal(msg, actual, expected) {
    const method = (typeof(expected) === 'object') ? 'deepEqual' : 'equal'
    try {
        if (typeof(actual) === 'function') actual = actual()
        assert[method](actual, expected, contextGroup+': '+msg)
        console.log(contextGroup+': '+msg+' 👍')
    } catch(err) {
        console.error(
            contextGroup+': '+msg+' 👎' +'\n'+
            'FAIL: '+ method +' '+ err.message +'\n'+
            (err.actual && 'Actual: '+ JSON.stringify(actual) +'\n' || '')+
            (err.expected && 'Expected: '+ JSON.stringify(expected) +'\n' || ''),
            err.stack || '- - -'
        )
        process.exit(1)
    }
}

/* * * T E S T S * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

contextGroup = 'Discover versioned step in a full path request'

equal(
    'Versioned feaf',
    ()=> findVersionedStep('/libA/libA.js'),
    '/libA/libA@.js'
)

equal(
    'Versioned dir',
    ()=> findVersionedStep('/libB/index.js'),
    '/libB@/index.js'
)

equal(
    'DVersioned dir on root level',
    ()=> findVersionedStep('/libC/dist/lib-back.js'),
    '/libC@/dist/lib-back.js'
)

equal(
    'Versioned dir inside another dir',
    ()=> findVersionedStep('/someDir/libD/index.js'),
    '/someDir/libD@/index.js'
)


contextGroup = 'Discover the real and compatible version'

equal(
    'Find file version without version notation',
    ()=> findVersionReturn('/libA/libA.js').data,
    '/libA/libA@1.2.3.js'
)

equal(
    'Find file version with defined minor',
    ()=> findVersionReturn('/libA/libA@1.1.js').data,
    '/libA/libA@1.1.1.js'
)

equal(
    'Find file version with defined minor and a wildcard',
    ()=> findVersionReturn('/libA/libA@1.1.*.js').data,
    '/libA/libA@1.1.1.js'
)


contextGroup = 'Find default file'

equal(
    'when requesting for its dir',
    ()=> responseReturn('/libB'),
    { code: 302, data: '/libB@1.1.2/index.js' }
)

equal(
    'when requesting for its dir with ending slash',
    ()=> responseReturn('/libB/').data,
    '/libB@1.1.2/index.js'
)

equal(
    'when requesting for its dir with version',
    ()=> responseReturn('/libB@1').data,
    '/libB@1.1.2/index.js'
)

equal(
    'when requesting for its dir with version and ending slash',
    ()=> responseReturn('/libB@1/').data,
    '/libB@1.1.2/index.js'
)


contextGroup = 'Find file defined in the pkg json'

equal(
    'when request for its dir without version',
    ()=> responseReturn('/libC').data,
    '/libC@3.0.0/dist/lib-front.js'
)

equal(
    'when request for its dir of vesrion 2',
    ()=> responseReturn('/libC@2').data,
    '/libC@2.0.0/lib.js'
)

equal(
    'when request for its dir of version 1',
    ()=> responseReturn('/libC@1').data,
    '/libC@1.0.0/main.js'
)


contextGroup = 'Unfindable files'

equal(
    'Cant mark an non versioned incomplete path',
    ()=> findVersionedStep('/libA'),
    '/libA@'
)

equal(
    'An incomplete path, without pkg json',
    ()=> responseReturn('/libA'),
    { code: 404, data: 'Version not found "/libA"' }
)

equal(
    'An non existent path',
    ()=> responseReturn('/nothing'),
    { code: 404, data: 'Version not found "/nothing"' }
)
