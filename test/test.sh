#!/bin/bash -e

PORT=8099
PROJECT_ROOT=$(realpath $(dirname $0)/..)
FIXTURES=$PROJECT_ROOT/test/fixtures
SPECS_DIR=$PROJECT_ROOT/test
DOCKER_TAG=$(./build --tag-only)
n='
'

if [[ " $@ " =~ ' --help ' ]]; then
    echo "
    Test the $DOCKER_TAG image.

    Usage:
    \$ $0 [options] [spec_file]

    Options:

    --no-rebuild      Do not run docker build before testing

    --test=<RegExp>   A test name filter to run. This will be prefixed with test_.*

    --docker-debug    Prints docker related output

    --help            Show this help message
    "
    exit 0
fi

function info() {
    echo -e "\e[0;32m>> $1\e[0m"
}

info 'Unit testing...'
if node $SPECS_DIR/unit.js; then
    echo -e "\e[0;32mUnit Tests Pass 😀\e[0m"
    echo ''
else
    echo -e "\e[0;31mUnit Tests Fail 😡\e[0m"
    exit 1
fi

info 'Behavior testing...'

[[ " $@ " =~ ' --docker-debug ' ]] && DOCKER_DEBUG=true || DOCKER_DEBUG=false

[[ " $@ " =~ ' --no-rebuild ' ]] || ./build

SPEC="${@: -1}"
test "${SPEC:0:1}" != '-' -a $# -gt 0 || SPEC=''

LAST_RES_HEADER=$(mktemp --suffix=-res-header-$DOCKER_TAG)
LAST_RES_BODY=$(mktemp --suffix=-res-body-$DOCKER_TAG)

SRV_HELPERS='
    function docker_debug() {
        [ $1 = OUT ] && COLOR=35 || COLOR=33
        if $DOCKER_DEBUG; then
            xargs -0 -I\{\} echo -e "\e[0;${COLOR}m{}\e[0m"
        fi
    }

    function run_server() {
        if test -z "$1"; then
            echo You must to set a port as first parameter >&2
            return 1
        fi
        docker run \
            -p $1:80 \
            -v $FIXTURES/sample-fs:/usr/share/nginx/html \
            -e CORS_RE="$2" \
            '$DOCKER_TAG' \
            1> >( docker_debug OUT ) \
            2> >( docker_debug ERR )
    }

    function wait_for_server_up() {
        if test -z "$1"; then
            echo You must to set a port as first parameter >&2
            return 1
        fi
        while ! curl http://localhost:$1 >/dev/null 2>&1; do
            sleep 0.2; echo -n 💤
        done
        echo ''
    }

    function stop_server() {
        if test -z "$1"; then
            echo You must to set a port as first parameter >&2
            return 1
        fi
        docker container ls |
        grep "$DOCKER_TAG.*:$1->80" |
        cut -d" " -f1 |
        xargs docker stop > /dev/null
    }
'

HELPERS='
    PORT='$PORT'
    LAST_RES_HEADER='$LAST_RES_HEADER'
    LAST_RES_BODY='$LAST_RES_BODY'
    FIXTURES='$FIXTURES'
    DOCKER_TAG='$DOCKER_TAG'
    DOCKER_DEBUG='$DOCKER_DEBUG'

    '$SRV_HELPERS'

    function req() {
        local URL="$1"; shift
        stderr=$(mktemp --suffix=-curl-err-'$DOCKER_TAG')
        echo -e "\e[0;1mRequire <$URL>...\e[0m"
        curl "$@" -D '$LAST_RES_HEADER' "$URL" 1>'$LAST_RES_BODY' 2>$stderr \
        && status=0 || status=$?
        echo -e "\e[0;31m$(cat $stderr)\e[0m"
        echo -e "\e[0;36m$(cat '$LAST_RES_HEADER')\e[0m"
        echo -e "\e[0;34m$(cat '$LAST_RES_BODY')\e[0m"
        rm $stderr
        return $status
    }

    function match() {
        test -f "$1" && VAL="$(cat $1)" || VAL="$1"
        if ( echo "$VAL" | egrep -q "$2" ); then
            echo -e "\e[0;32m\"$2\" match 👍\e[0m"
            return 0
        else
            echo -e "\e[0;31m\"$2\" does not match \"$VAL\"\e[0m"
            exit 1
        fi
    }

    function not_match() {
        test -f "$1" && VAL="$(cat $1)" || VAL="$1"
        if ! ( echo "$VAL" | egrep -q "$2" ); then
            echo -e "\e[0;32m\"$2\" match 👍\e[0m"
            return 0
        else
            echo -e "\e[0;31m\"$2\" does not match \"$VAL\"\e[0m"
            exit 1
        fi
    }
'

eval "$SRV_HELPERS"
run_server $PORT '^https?:\/\/localhost(:\d+)?$' &

wait_for_server_up $PORT
info 'Server is up.'

declare TEST_SUCCESS=0
declare TEST_FAIL=0

GET_FILTER='.* --test=([^ ]+) .*'
TEST_FILTER=$(echo " $@ " | egrep "$GET_FILTER" | sed -r "s/$GET_FILTER/\1/")
TEST_FILTER="^ *function +test_([A-Za-z0-9_]*$TEST_FILTER[A-Za-z0-9_]*)(\(\))? *\{.*"

hr=''; for i in $(seq $(tput cols)); do hr="$hr═"; done

info "running tests..."
for spec in ${SPEC:-$SPECS_DIR/*.spec.sh}; do
    echo -e "\n$hr\n"
    spec_name="$(basename ${spec%.spec.sh} | tr '_' ' ')"
    info "Running spec \"$spec_name\"..."
    while read test_line; do
        test=$(echo $test_line | sed -r "s/.*$TEST_FILTER.*/\1/")
        echo -e "\n$hr\n"
        echo ">> Spec $spec_name: Test $(echo $test | tr '_' ' '):"
        if bash -c "set -e$n$HELPERS. $spec$n test_$test"; then
            TEST_SUCCESS=$((TEST_SUCCESS + 1))
            echo -e "\e[0;32mTest Pass 😀\e[0m"
        else
            TEST_FAIL=$((TEST_FAIL + 1))
            echo -e "\e[0;31mTest Fail 😡\e[0m"
        fi
    done < <(cat $spec | egrep "$TEST_FILTER")
done

echo -e "\n$hr\n"
rm $LAST_RES_HEADER $LAST_RES_BODY

info 'Stoping server container...'
stop_server $PORT

if [ $TEST_FAIL = 0 ]; then
    echo -e "
    Success: \e[0;32m$TEST_SUCCESS\e[0m
    Fails:   \e[0;32m$TEST_FAIL\e[0m
    "
    exit 0
else
    echo -e "
    Success: \e[0;32m$TEST_SUCCESS\e[0m
    Fails:   \e[0;31m$TEST_FAIL\e[0m
    "
    exit 1
fi
