function test_response_includes_server_version_header() {
    req "http://localhost:$PORT/libA/libA.js"
    match $LAST_RES_HEADER 'SemVer-Server: 0.1.1'
}
