
function test_redirect_without_wildcard() {
    req "http://localhost:$PORT/libA/libA.js"
    match $LAST_RES_HEADER 'HTTP/1.1 302'
    match $LAST_RES_HEADER 'Location: /libA/libA@1.2.3.js'
}

function test_redirect_major_with_wildcard() {
    req "http://localhost:$PORT/libA/libA@1.*.js"
    match $LAST_RES_HEADER 'HTTP/1.1 302'
    match $LAST_RES_HEADER 'Location: /libA/libA@1.2.3.js'
}

function test_redirect_major_without_wildcard() {
    req "http://localhost:$PORT/libA/libA@1.js"
    match $LAST_RES_HEADER 'HTTP/1.1 302'
    match $LAST_RES_HEADER 'Location: /libA/libA@1.2.3.js'
    req "http://localhost:$PORT/libC@2/style.css"
    match $LAST_RES_HEADER 'HTTP/1.1 302'
    match $LAST_RES_HEADER 'Location: /libC@2.0.0/style.css'
    req "http://localhost:$PORT/someDir/libD@1/index.js"
    match $LAST_RES_HEADER 'HTTP/1.1 302'
    match $LAST_RES_HEADER 'Location: /someDir/libD@1.0.0/index.js'
}

function test_redirect_with_old_minor() {
    req "http://localhost:$PORT/libA/libA@1.1.*.js"
    match $LAST_RES_HEADER 'HTTP/1.1 302'
    match $LAST_RES_HEADER 'Location: /libA/libA@1.1.1.js'
}

function test_redirect_with_old_minor_without_wildcard() {
    req "http://localhost:$PORT/libA/libA@1.1.js"
    match $LAST_RES_HEADER 'HTTP/1.1 302'
    match $LAST_RES_HEADER 'Location: /libA/libA@1.1.1.js'
}

function test_direct_request() {
    req "http://localhost:$PORT/libA/libA@1.2.3.js"
    match $LAST_RES_HEADER 'HTTP/1.1 200'
    match $LAST_RES_BODY 'libA-1.2.3'
    req "http://localhost:$PORT/libA/libA@1.1.0.js"
    match $LAST_RES_HEADER 'HTTP/1.1 200'
    match $LAST_RES_BODY 'libA-1.1.0'
    req "http://localhost:$PORT/libB@1.1.0/index.js"
    match $LAST_RES_HEADER 'HTTP/1.1 200'
    match $LAST_RES_BODY 'libB-1.1.0'
    req "http://localhost:$PORT/libC@2.0.0/style.css"
    match $LAST_RES_HEADER 'HTTP/1.1 200'
    match $LAST_RES_BODY 'libC style 2.0.0'
    req "http://localhost:$PORT/someDir/libD@1.0.0/index.js"
    match $LAST_RES_HEADER 'HTTP/1.1 200'
    match $LAST_RES_BODY 'libD-1.0.0'
}

function test_redirect_ignore_query() {
    req "http://localhost:$PORT/libA/libA@1.*.js?abc=123"
    match $LAST_RES_HEADER 'HTTP/1.1 302'
    match $LAST_RES_HEADER 'Location: /libA/libA@1.2.3.js'
}
