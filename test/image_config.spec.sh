
function test_success_CORS_with_direct_path() {
    local PORT=3333
    run_server $PORT '^https?:\/\/localhost(:\d+)?$' &
    wait_for_server_up $PORT
    req "http://localhost:$PORT/libA/libA@1.2.3.js" \
        -H "Origin: http://localhost:$PORT"
    stop_server $PORT
    match $LAST_RES_HEADER "Access-Control-Allow-Origin: http://localhost:$PORT"
    match $LAST_RES_HEADER 'Vary: Origin'
    match $LAST_RES_HEADER 'HTTP/1.1 200'
}

function test_success_CORS_with_redirect() {
    local PORT=3333
    run_server $PORT '^https?:\/\/localhost(:\d+)?$' &
    wait_for_server_up $PORT
    req "http://localhost:$PORT/libA/libA.js" \
        -H "Origin: http://localhost:$PORT"
    stop_server $PORT
    match $LAST_RES_HEADER "Access-Control-Allow-Origin: http://localhost:$PORT"
    match $LAST_RES_HEADER 'Vary: Origin'
    match $LAST_RES_HEADER 'HTTP/1.1 302'
}

function test_fail_CORS_with_direct_path() {
    local PORT=3333
    run_server $PORT '^https?:\/\/localhost(:\d+)?$' &
    wait_for_server_up $PORT
    req "http://localhost:$PORT/libA/libA@1.2.3.js" \
        -H "Origin: https://example.com"
    stop_server $PORT
    match $LAST_RES_HEADER 'Access-Control-Allow-Origin: not for you'
    match $LAST_RES_HEADER 'Vary: Origin'
    match $LAST_RES_HEADER 'HTTP/1.1 200'
}

function test_fail_CORS_with_redirect() {
    local PORT=3333
    run_server $PORT '^https?:\/\/localhost(:\d+)?$' &
    wait_for_server_up $PORT
    req "http://localhost:$PORT/libA/libA.js" \
        -H "Origin: http://example.com"
    stop_server $PORT
    match $LAST_RES_HEADER 'Access-Control-Allow-Origin: not for you'
    match $LAST_RES_HEADER 'Vary: Origin'
    match $LAST_RES_HEADER 'HTTP/1.1 302'
}
