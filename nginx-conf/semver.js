'use strict';

/*
SemVer NJS Module
Copyright (C) 2022 nTopus.com.br

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

const fs = require('fs');
const root = '/usr/share/nginx/html'
const extRE = /(\.[^.]+)$/
const getVersion = '@[0-9]+\\.[0-9]+\\.[0-9]+'

/**
 * Dev's debuging ultimate weapon
 */
function debug() {
    if (typeof(console) === 'object') {
        const args = [].concat.apply(['>>'], arguments)
        console.log.apply(console, args)
    }
}

/**
 * Finds what step in the path is the the versioning, than mark it with an "@".
 */
function findVersionedStep(path) {
    const pathMainPieces = path.match(/(.*\/)([^\/]+)$/)
    let dir, file
    if (pathMainPieces) {
        dir = pathMainPieces[1]
        file = pathMainPieces[2]
    }
    try {
        // Try to confirm if the version is on the file.
        fs.accessSync(root + dir, fs.constants.R_OK)
        return dir + file.replace(/(\.[^.]+|\/|)$/, '@$1')
    } catch(err) {
        // The version is in some step of the path...
        let curPath = '', found = false
        path.substring(1).split('/').forEach(step => {
            curPath += '/' + step
            if (found) return null
            try {
                fs.accessSync(root + curPath, fs.constants.R_OK)
            } catch(err) {
                found = true
                curPath += '@'
            }
        })
        return curPath
    }
}

// TODO: modularize this function!
// TODO: find version with <pre-release> and <build> notations.
function findVersion(r) {
    if (!r.path.match(/[^\/]@/)) r.path = findVersionedStep(r.path)

    const fullVersion = /@([0-9]+|\*)\.([0-9]+|\*)\.([0-9]+|\*)/
    const minorVersion = /(@([0-9]+|\*)\.([0-9]+|\*))/
    const majorVersion = /(@([0-9]+|\*))/

    const path = r.path.split('/')
    const versionedStepIndex = path.findIndex(p => p.match(/@/))
    const prevPath = path.slice(0, versionedStepIndex).join('/')
    const postPath = path.slice(versionedStepIndex+1).join('/')
    let versionedStep = path[versionedStepIndex]

    if (versionedStep.match(fullVersion)) {
        /* All right */
    } else if (versionedStep.match(minorVersion)) {
        versionedStep = versionedStep.replace(minorVersion, '$1.*')
    } else if (versionedStep.match(majorVersion)) {
        versionedStep = versionedStep.replace(majorVersion, '$1.*.*')
    } else {
        versionedStep = versionedStep.replace(/@/, '@*.*.*')
    }
    const stepRE = new RegExp(
        versionedStep
        .replace(/([[{}()?.+^$\\]|\])/g, '\\$1')
        .replace(/\*/g, '[0-9]+')
    )

    const realVerStep = fs.readdirSync(root + prevPath)
                          .filter(file => file.match(stepRE))
                          .sort().reverse()[0]
    if (realVerStep) {
        let versionedPath = prevPath + '/' + realVerStep
        if (postPath) versionedPath += '/' + postPath
        return versionedPath
    } else {
        return false
    }
  }

  function findVersionFile(r) {
    let curPath = findVersion(r)
    if (curPath) {
        try {
            const realPath = fs.realpathSync(root + curPath).toString()
            const state = fs.statSync(realPath)
            if (state.isDirectory()) {
                curPath = discoverFile(curPath)
            }
            // TODO: add cache expiration header, configurable by env var
            // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Expires
            // TODO: add etag based on version.
            return r.return(302, curPath)
        } catch(err) {
            /* It is fine. It will reach the throw bellow. */
        }
    }
    throw {
        message: 'Version not found',
        code: 'ENOENT'
    }
}

// TODO: Support package.json definition
function discoverFile(dir) {
    try {
        let path = dir + '/'
        const pkg = JSON.parse(fs.readFileSync(root+path+'package.json', 'utf8'))
        path += pkg.browser ? pkg.browser : pkg.main
        path = path.replace(/\/\.\/+/g, '/').replace(/\/+/g, '/')
        fs.accessSync(root + path, fs.constants.R_OK)
        return path
    } catch(err) {
        fs.accessSync(root + dir + '/index.js', fs.constants.R_OK)
        return dir + '/index.js'
    }
}

// TODO: cache response
function response(r) {
    try {

        r.path = r.uri.replace(/\?.*/, '').replace(/\/+$/, '')
        r.path = r.path || '/'
        if (r.path === '/') redirectBrowseRoot(r)
        else findVersionFile(r)

    } catch(err) {

        if ( err.code === 'ENOENT' ) {
            r.return(404, `${err.message} "${r.uri}"`)
        } else {
            throw err
        }

    }
}

function redirectBrowseRoot(r) {
    r.return(302, '/browse')
}

export default { findVersionedStep, findVersion, findVersionFile, response }
