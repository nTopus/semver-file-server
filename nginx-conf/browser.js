/*
File Browser NJS Module
Copyright (C) 2022 nTopus.com.br

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

function notFound(r, message) {
    return r.return(404, buildPage('404 Not Found '+r.pathNoPrefix, message))
}

function updateReqPathInfo(r, uri) {
    r.pathNoPrefix = uri
                      .replace(/^\/browse\/?/, '/') // Remove browsing prefix
                      .replace(/\?.*/, '')          // Remove query
    r.path = r.pathNoPrefix
              .replace(/\/+$/, '')          // Remove tail bar
              .replace(/^\s*$/, '/')        // Ensure "/" if its empty
}

function browse(r) {
    r.headersOut['Content-Type'] = 'text/html; charset=utf-8'
    updateReqPathInfo(r, r.uri)
    let state, realPath
    try {
        realPath = fs.realpathSync(root + r.path).toString()
        state = fs.statSync(realPath)
    } catch(err1) {
        const versionedPath = findVersion(r).toString()
        if (!versionedPath) {
            return notFound(r, '(No versioned path)')
        }
        try {
            realPath = fs.realpathSync(root + versionedPath).toString()
            updateReqPathInfo(r, versionedPath)
            state = fs.statSync(realPath)
        } catch(err2) {
            return notFound(r, err2.message)
        }
    }
    if (state.isDirectory()) {
        r.return(200, buildPage(r.pathNoPrefix, fileList(r, realPath)))
    } else {
        r.return(200, buildPage(r.pathNoPrefix, showFile(r, realPath)))
    }
}

const NONVER_LABEL = 'nonversioned'

function listItem(dir, file, versions, icon) {
  const isVersioned = !!(versions.length > 1 || versions[0] !== NONVER_LABEL)
  const sufix = (icon === '📁') ? '/' : ''
  if (isVersioned) {
      const last = versions[versions.length-1]
      return `<li>
          <a class="basename" href="${dir+file}@${last}"><i>${icon}</i> ${file}</a>
          ${versions.map(ver =>
              `<a class="version" href="${dir+file}@${ver}">${ver}</a>`
          ).join(' ')}
      </li>`
  } else {
      return `<li>
          <a class="basename" href="${icon==='⬆️' ? dir : dir+file}"><i>${icon}</i> ${file}</a>
      </li>`
  }
}

function fileList(r, path) {
    let html = '<ul id="file-list">'
    try {
        const dir = ('/browse' + r.path).replace(/\/*$/, '/')
        if (dir !== '/browse/') {
            html += listItem(dir.replace(/\/[^\/]+\/?$/, ''), 'parent', [NONVER_LABEL], '⬆️')
        }
        const files = fs.readdirSync(path).sort()
        .map(f => {
            const match = f.match(/^(.*)@([0-9]+\.[0-9]+\.[0-9]+[^.]*)/) || [,f]
            const baseName = match[1]
            const versions = match[2] ? [match[2]] : [NONVER_LABEL]
            return { name: f, baseName, versions }
        })
        .filter((f, i, arr)=> {
            const firstIdx = arr.findIndex(f2 => f2.baseName === f.baseName)
            if (firstIdx === i) {
                return true
            } else {
                const first = arr[firstIdx]
                first.versions.push(f.versions[0])
                return false
            }
        })
        files.forEach(file => {
            const state = fs.statSync(path +'/'+ file.name)
            let icon = state.isDirectory() ? '📁' : '📄'
            html += listItem(dir, file.baseName, file.versions, icon)
        })
    } catch (err) {
        html += `<br>fileList(Req&lt;${r.path}>, "${path}") fail!` +
                '<br>' + err.stack.split('\n').join('<br>')
    }
    return html + '</ul>'
}

function showFile(r, path) {
    const parent = ('/browse' + r.path).replace(/\/[^\/]+\/?$/, '')
    return `
    <article id="show-file">
    <a class="basename" href="${parent}"><i>⬆️</i> parent</a>
    <pre id="file-content">${
        fs.readFileSync(path, 'utf8')
        .replace(/&/gm, '&amp;')
        .replace(/</gm, '&lt;')
        .replace(/>/gm, '&gt;')
    }</pre>
    </article>`
}

export default { browse }
