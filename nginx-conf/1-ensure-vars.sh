#!/bin/sh -e

echo "CORS_RE: \"$CORS_RE\""

if test -z "$CORS_RE"; then
    echo "
    You MUST define the env var CORS_RE to filter allowed domains.
    The \"Allow All\" regexp is \".*\"
    " >&2
    exit 1
fi

if (echo "$CORS_RE" | egrep -q '\s'); then
    echo "
    You MUST NOT use spaces in the env var CORS_RE.
    " >&2
    exit 1
fi
